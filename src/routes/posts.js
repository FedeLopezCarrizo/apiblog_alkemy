const express = require('express');
const router = express.Router();
//requiero el controller de post
const postsController = require('../controllers/postsController');
//requiero el middleware de validacion 
const validator = require('../middlewares/validator');

//router para mostrar un listado de post ordenados por fecha de creacion
router.get('/', postsController.all);

//router para mostrar un post especifico
router.get('/:id', postsController.detail);

//router para guardar un nuevo post con un middleware para validar datos
router.post('/', validator.create, postsController.create);

//router para actualizar el post con un id pasado por parametro
router.patch('/:id', validator.create, postsController.update);

//router para eliminar un post con un id pasado por parametro
router.delete('/:id', postsController.delete);

module.exports = router;