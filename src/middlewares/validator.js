const { body } = require('express-validator');

module.exports = {
    create: [
        body('titulo')
            .notEmpty()
            .withMessage('El campo titulo es obligatorio')
            .isLength({ max: 500 })
            .withMessage('El campo titulo tiene un máximo de 500 caracteres'),
        body('contenido')
            .notEmpty()
            .withMessage('El campo contenido es obligatorio'),
        body('imagen')
            .notEmpty()
            .withMessage('El campo imagen es obligatorio')
            .custom(function(value, { req }){
                let imagenDividida = req.body.imagen.split('/');
                if (imagenDividida.length > 0)
                {
                    let urlImagen = imagenDividida[imagenDividida.length - 1].split('.');
                    let extensionImagen = urlImagen[urlImagen.length - 1];

                    if(extensionImagen == 'jpg' || extensionImagen == 'jpeg' || extensionImagen == 'png'){
                        return true;
                    }
                }
                return false;
                
            })
            .withMessage('La extensión de la imagen es incorrecta'),
        body('categoriaId')
            .notEmpty()
            .withMessage('El campo categoria es obligatorio')
            .isInt()
            .withMessage('El campo categoria tiene que ser un número entero'),
        body('fechaCreacion')
            .isDate()
            .withMessage('El campo fecha no es válido')
    ]
}