const { validationResult } = require('express-validator');
const { Post } = require('../database/models');

const controller = {
    all: async (req, res) =>{
        try {
            const posts = await Post.findAll({
                attributes: ['id', 'titulo', 'imagen', 'fechaCreacion'],
                order: [['fechaCreacion', 'DESC']],
                include: [ { association: "categorias" } ]
            });

            let cantidadRegistros = Object.keys(posts).length;
            let respuesta;

            if (cantidadRegistros > 0){
                respuesta = {
                    metadata:{
                        status: 200,
                        cantidad: cantidadRegistros
                    },
                    resultados: posts
                }
            }else{
                respuesta = {
                    metadata:{
                        status: 204
                    },
                    resultados: "La cantidad de registros es cero."
                }
            }

            res.json(respuesta);
        } catch (error) {
            console.log(error);
        }
    },
    detail: async (req, res) => {
        try {
            let postDetail = await Post.findByPk(req.params.id, { include: ['categorias']});

            if (postDetail != null){
                respuesta = {
                    metadata:{
                        status: 200
                    },
                    resultados: postDetail
                }
            }else{
                respuesta = {
                    metadata:{
                        status: 204
                    },
                    resultados: "El id utilizado no es válido"
                }
            }

            res.json(respuesta);
        } catch (error) {
            console.log(error);
        }
    },
    create: async (req, res) => {
        try {
            let errores = validationResult(req);
            
            if (errores.isEmpty()){
                let newPost = {
                    ...req.body
                }

                await Post.create(newPost);
                res.json(newPost);
            }else{
                res.json(errores);
            }
            
        } catch (error) {
            console.log(error);
        }
    },
    update: async(req, res) => {
        try {
            const postId = req.params.id;
            const updatePost = await Post.findByPk(postId);

            if (updatePost != null){
                let updatePostBody = {
                    ...req.body
                }              
                await updatePost.update(updatePostBody);

                respuesta = {
                    metadata:{
                        status: 200
                    },
                    resultados: updatePost
                }
            }else{
                respuesta = {
                    metadata:{
                        status: 204
                    },
                    resultados: "El id utilizado no es válido"
                }
            }

            res.json(respuesta);
        } catch (error) {
            console.log(error);
        }
    },
    delete: async (req, res) => {
        try {
            const postId = req.params.id;
            const postToDelete = await Post.findByPk(postId);

            if (postToDelete != null){
                await postToDelete.destroy({
                    where: {
                        id: postId
                    }
                });

                respuesta = {
                    metadata:{
                        status: 200
                    },
                    resultados: "El id " + postId + " fue eliminado con éxito"
                }
            }else{
                respuesta = {
                    metadata:{
                        status: 204
                    },
                    resultados: "El id utilizado no es válido"
                }
            }

            res.json(respuesta);
        } catch (error) {
            console.log(error);
        }
    }
};

module.exports = controller;