module.exports = (sequelize, DataTypes) => {
    const Categoria = sequelize.define("Categoria", {
        nombre: DataTypes.STRING
    },
    {
        tableName: 'categorias'
    });
    
    Categoria.associate = (models => {
        Categoria.hasMany(models.Post, {
            as: "posts",
            foreignKey: "categoriaId"
        });
    });

    return Categoria;
}