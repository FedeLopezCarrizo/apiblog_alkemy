module.exports = (sequelize, DataTypes) => {
    const Post = sequelize.define("Post", {
        titulo: DataTypes.STRING,
        contenido: DataTypes.TEXT,
        imagen: DataTypes.TEXT,
        categoriaId: DataTypes.INTEGER,
        fechaCreacion: DataTypes.DATE
    });
    Post.associate = (models => {
        Post.belongsTo(models.Categoria, {
            as: "categorias",
            foreignKey: "categoriaId"
        });
    });

    return Post;
}